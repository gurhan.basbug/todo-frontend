// Add task to list DIV
function addToList (task)
{
 if (!this.listDiv)	// Make static
  this.listDiv = document.getElementById ('list');

 // Create task LI element
 let listLi = document.createElement ('LI');
 listLi.innerHTML = task;
 // Append LI element to list DIV
 this.listDiv.appendChild (listLi);

 // Scroll to latest
 listDiv.scrollTo (0, listDiv.scrollHeight);
}

// Iterate tasks in JSON and call addToList function
function tileList (json)
{
 if (!json.tasks)
  return false;

 for (task of json.tasks)
  addToList (task);
}

// Fetch tasks from backend
function getList ()
{
 // Go to backend
 let oReq = new XMLHttpRequest ();
 oReq.addEventListener ('load', (data) =>
	{
	 let json = null;
	 try { json = JSON.parse (data.target.responseText); }
	 catch (err) { console.log ('Backend communication error: ' + err); }

	 if (json && json.status == 'ok')
	 {
	  tileList (json);
	  console.log (json.message);
	 }
	}
 );
 oReq.open ('GET', location.protocol + '//' + location.host + ':8080/ws.php?c=get');
 oReq.send ();
}

// Add button click event
function todo_btn_Clicked (oBtn)
{
 let oForm = oBtn.form;
 let oTxt = oForm['todo_txt'];
 oTxt.value = oTxt.value.trim ();
 
 if (!oForm.checkValidity ())
  return false;

 // New task to be added
 let task = oTxt.value;

 // Go to backend
 let oReq = new XMLHttpRequest ();
 oReq.addEventListener ('load', (data) =>
	{
	 let json = null;
	 try { json = JSON.parse (data.target.responseText); }
	 catch (err) { console.log ('Backend communication error: ' + err); }

	 if (json && json.status == 'ok')
	 {
	  addToList (task);
	  setTimeout (() => {oForm.reset ();}, 100);
	  console.log (json.message);
	 }
	}
 );
 oReq.open ('GET', location.protocol + '//' + location.host + ':8080/ws.php?c=set&task=' + window.btoa (task));
 oReq.send ();
}
